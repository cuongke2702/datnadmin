import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { getUserAPIRequest, updateUserAPICall } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'
import jwt_decode from 'jwt-decode';
import { Redirect } from 'react-router-dom';


class UpdateUser extends Component {

  constructor(props) {
    super(props);
    this.setState({
      userId: '',

    })
  }
  componentDidMount() {
    if(!this.props.isLoggedIn.isLoggedIn){
      return <Redirect to="/admin/login"/>;
  }
    const { match } = this.props;
    this.props.getUserById(match.params.userId);
    this.setState({
      userId: match.params.userId
    })
  }

  // handleChange = (event) => {
  //     const target = event.target;
  //     const value = target.value;
  //     const name = target.name;
  //     console.log(this.state.user);
  //     this.setState({
  //       user :{
  //         [name]: value,

  //       }
  //     });
  //   };

  handelsubmit = (event) => {
    event.preventDefault();
    const user = {
      userId: this.state.userId,
      fullName: event.target.fullName.value,
      phone: event.target.phone.value,
      address: event.target.address.value,
      email: event.target.email.value,
    }
    this.props.onUpdateUser(user);
    this.props.history.push('/admin/user/index')
  }

  render() {
    const { editReducer } = this.props
    const { edit } = editReducer
    return (
      <div>
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform" >
          <Form onSubmit={this.handelsubmit}>
            <FormGroup>
              <Label for="address">Full name</Label>
              <Input
                type="text"
                name="fullName"
                id="fullName"
                defaultValue={edit.fullName}
              />
            </FormGroup>
            <FormGroup>
              <Label for="address">Phone</Label>
              <Input
                type="text"
                name="phone"
                id="phone"
                defaultValue={edit.phone}
              />
            </FormGroup>
            <FormGroup>
              <Label for="address">address</Label>
              <Input
                type="text"
                name="address"
                id="address"
                defaultValue={edit.address}
              />
            </FormGroup>
            <FormGroup>
              <Label for="address">Email</Label>
              <Input
                type="text"
                name="email"
                id="email"
                defaultValue={edit.email}
              />
            </FormGroup>
            <FormGroup>
              <Button color="primary" type="submit">
                Save
                        </Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    getUserById: (id) => {
      dispatch(getUserAPIRequest(id))
    },
    onUpdateUser: (user) => {
      dispatch(updateUserAPICall(user))
    }

  }
}

const mapStateToProps = state => {
  return {
    editReducer: state,
    isLoggedIn : state.auth
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateUser);