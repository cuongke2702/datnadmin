import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormText, FormGroup, Input, Label } from "reactstrap";
import { addProductApi, getAllProductAPI, getCartAdminCallApi, getDetailProductApi, updateProductApi } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'
import { MenuItem, Select } from '@material-ui/core';
import jwt_decode from 'jwt-decode';
import { Redirect } from 'react-router-dom';

class ProductEdit extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // productId: '',
      productName: '',
      detail: '',
      price: '',
      quantity: '',
      description: '',
      catId: this.props?.productReducer?.product?.product?.catId,
      User_Id: '',
    }
  }

  componentDidMount() {
    if (!this.props.isLoggedIn.isLoggedIn) {
      return <Redirect to="/admin/login" />;
    }
    const { match } = this.props
    this.props.getProductById(match.params.productId);
    this.props.GetAllCat();
  }

  changeCat = (e) => {
    this.setState({ catId: e.target.value });
  }

  handelsubmit = (event) => {
    event.preventDefault();
    const productJson = {
      // productId: event.target.productId.value,
      productName: event.target.productName.value,
      detail: event.target.detail.value,
      price: event.target.price.value,
      quantity: event.target.quantity.value,
      description: event.target.description.value,
      catId: this.state.catId,
      User_Id: 1
    }
    let formData = new FormData();
    formData.append('json', JSON.stringify(productJson))
    const image = event.target.FileTong.files
    const thumnails = event.target.myFile.files
    for (let i = 0; i < image.length; i++) {
      formData.append('FileTong', image[i])
    }
    for (let i = 0; i < thumnails.length; i++) {
      formData.append('myFile', thumnails[i])
    }
    this.props.updateProduct(formData, this.props.productReducer.product?.product?.productId);
    this.props.history.push("/admin/product/index");
  }

  render() {
    const { productReducer } = this.props;
    const { product, cat } = productReducer
    return (
      <div>
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform" >
          <Form onSubmit={this.handelsubmit} style={{ width: '95%', marginTop: '50px' }}>
            {/* <FormGroup>
              <Label for="productId">Product ID</Label>
              <Input
                type="text"
                name="productId"
                id="productId"
                defaultValue={product.productId}
              />
            </FormGroup> */}
            <FormGroup>
              <Label for="productName">Product Name</Label>
              <Input
                type="text"
                name="productName"
                id="productName"
                defaultValue={product?.product?.productName}
              />
            </FormGroup>
            <FormGroup>
              <Label for="description">Description</Label>
              <Input
                type="text"
                name="description"
                id="description"
                defaultValue={product?.product?.description}
              />
            </FormGroup>
            <FormGroup>
              <Label for="quantity">quantity</Label>
              <Input
                type="text"
                name="quantity"
                id="quantity"
                defaultValue={product?.product?.quantity}
              />
            </FormGroup>
            <FormGroup>
              <Label for="price">price</Label>
              <Input
                type="text"
                name="price"
                id="price"
                defaultValue={product?.product?.price}
              />
            </FormGroup>
            <FormGroup>
              <Label for="catId">Category</Label>
              <Select key={product?.product?.catId} defaultValue={product?.product?.catId} style={{ width: '100%' }} type="select" name="catId" id="catId" onChange={this.changeCat}>
                {
                  productReducer.cat.map(data => (
                    <MenuItem value={data.catId}>{data.catName}</MenuItem>
                  ))
                }
              </Select>
            </FormGroup>
            <FormGroup>
              <Label for="myfile">Thumnails</Label>
              <Input type="file" multiple name="myFile" id="myfile" defaultValue={product?.product?.thumbnail} />
            </FormGroup>
            <FormGroup>
              <Label for="FileTong">Images</Label>
              <Input type="file" name="FileTong" id="FileTong" defaultValue={product?.product?.images} />
            </FormGroup>
            <FormGroup>
              <Label for="detail">Detail</Label>
              <Input type="textarea" name="detail" id="detail" defaultValue={product?.product?.detail} />
            </FormGroup>
            <FormGroup>
              <Button color="primary" type="submit">
                Save
                        </Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAPIproduct: () => {
      dispatch(getAllProductAPI())
    },
    getProductById: (productId) => {
      dispatch(getDetailProductApi(productId))
    },
    updateProduct: (formData, id) => {
      dispatch(updateProductApi(formData, id));
    },
    GetAllCat: () => {
      dispatch(getCartAdminCallApi())
    },
  }
}

const mapStateToProps = state => {
  return {
    productReducer: state,
    isLoggedIn: state.auth
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductEdit);