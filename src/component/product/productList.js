import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllProductAPI, deleteCatAPI, deleteProductApi, getPagitions,getNumberAPI } from '../../actions';
import { Button} from "reactstrap";
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css';
import { Redirect } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

class ProductList extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (!this.props.isLoggedIn.isLoggedIn) {
            return <Redirect to="/admin/login" />;
        }
        this.props.getProductPagination(1);
        this.props.getCountProduct()
    }

    delete(productId) {
        this.props.deleteProduct(productId)
    }


    handlePageChange = (event) => {
        let number=event.selected
        this.props.getProductPagination(number)
	};

    render() {

        const { number,productReducer } = this.props;
        var totalProduct=productReducer?.number;
        var pageNumber=totalProduct/40;
        return (
            <div className="App">
                <DashboardNavbar />
                <DashboardSidebar />
                <div className="divform" >
                    <a style={{ marginTop: '100px', marginRight: '1170px', width: '200px' }} className="btn btn-primary" href={"/admin/product/add"}>Thêm sản phẩm</a>
                    <a style={{ marginTop: '-66px',marginLeft:'210px', marginRight: '1170px', width: '200px' }} className="btn btn-primary" href={"http://localhost:8080/api/csv/productdownload"}>Download Product</a>
                    <a style={{ marginTop: '-114px',marginLeft:'423px', marginRight: '1170px', width: '200px' }} className="btn btn-primary" href={"http://localhost:8080/api/csv/commentdownload"}>Download Comment</a>
                    <table style={{ width: '95%', marginTop: '10px' }} id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
                        <thead>
                            <tr>
                                <th className="text-center">Id</th>
                                <th className="text-center">Tên sản phẩm</th>
                                <th className="text-center">Giá sản phẩm</th>
                                <th className="text-center">Số lượng</th>
                                <th className="text-center">Hình ảnh</th>
                                <th className="text-center" colSpan={2}>Chức năng</th>
                                {/* <th className="text-center">Add</th> */}
                               
                            </tr>
                        </thead>
                        <tbody>
                            {
                                productReducer.product.map(data => (
                                    <tr align="start">
                                        <td className="text-center">{data.productId}</td>
                                        <td className="text-center">{data.productName}</td>
                                        <td className="text-center">{data.price}</td>
                                        <td className="text-center">{data.quantity}</td>
                                        <td className="text-center">
                                            {
                                                data.images!=null?
                                            data.images.startsWith("http")==true && data.images!=null?<img className="img-selected mx-auto" style={{height:'300px',width:'300px'}} src={data.images} alt="" />
                                            :<img src={`../../images/`+data.images} style={{height:'300px',width:'300px'}}/>:<></>
                                            }
                                        </td>
                                        <td className="text-center">
                                            <a className="btn btn-primary" href={"/admin/product/edit/" + data.productId} style={{width: '100px'}}>Cập nhật</a>
                                        </td>
                                        {/* <td className="text-center">
                                            <a className="btn btn-primary" href="/admin/product/add">Add</a>
                                        </td> */}
                                        <td className="text-center">
                                            <button type="button" className="btn btn-danger" onClick={() => this.delete(data.productId)}>Xóa</button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                    <ReactPaginate
                        breakLabel="..."
                        nextLabel="next >"
                        onPageChange={this.handlePageChange}
                        pageRangeDisplayed={5}
                        pageCount={pageNumber}
                        previousLabel="< previous"
                        renderOnZeroPageCount={null}
                        containerClassName="pagination justify-content-center"
                        pageClassName="page-item"
                        pageLinkClassName="page-link"
                        previousClassName="page-item"
                        previousLinkClassName="page-link"
                        nextClassName="page-item"
                        nextLinkClassName="page-link"
                        activeClassName="active"
                    />
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        productReducer: state,
        isLoggedIn: state.auth
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAPIproduct: () => {
            dispatch(getAllProductAPI())
        },
        deleteProduct: (id) => {
            dispatch(deleteProductApi(id))
        },
        getProductPagination : (number)=> {
            dispatch(getPagitions(number))
        },
        getCountProduct:()=> {
            dispatch(getNumberAPI())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);