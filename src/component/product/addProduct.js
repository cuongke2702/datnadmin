import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormText, FormGroup, Input, Label } from "reactstrap";
import { addProductApi, getCartAdminCallApi } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'
import { MenuItem, Select } from '@material-ui/core';
import jwt_decode from 'jwt-decode';
import { Redirect } from 'react-router-dom';

class ProductAdd extends Component {

    constructor(props) {
        super(props);
        this.setState({
            catId: this.props?.catReducer?.cat[0]?.catId,
            catName: '',
            disable: false
        })
    }

    componentDidMount() {
        if(!this.props.isLoggedIn.isLoggedIn){
            return <Redirect to="/admin/login"/>;
        }
        this.props.GetAllCat();
    }

    changeCat = (e) => {
        console.log(e.target.value);
        this.setState({ catId: e.target.value })
    }

    handelsubmit = (event) => {
        event.preventDefault();
        const productJson = {
            productId: event.target.productId.value,
            productName: event.target.productName.value,
            detail: event.target.detail.value,
            price: event.target.price.value,
            quantity: event.target.quantity.value,
            description: event.target.description.value,
            catId: this.state.catId,
            User_Id: 1
        }
        let formData = new FormData();
        formData.append('json', JSON.stringify(productJson))
        const image = event.target.FileTong.files
        const thumnails = event.target.myFile.files
        for (let i = 0; i < image.length; i++) {
            formData.append('FileTong', image[i])
        }
        for (let i = 0; i < thumnails.length; i++) {
            formData.append('myFile', thumnails[i])
        }
        this.props.addProduct(formData);
        this.props.history.push("/admin/product/index")
    }

    render() {
        const { catReducer } = this.props
        return (
            <div>
                <DashboardNavbar />
                <DashboardSidebar />
                <div className="divform" >
                    <Form onSubmit={this.handelsubmit} style={{ width: '95%', marginTop: '50px' }}>
                        <FormGroup>
                            <Label for="productId">Product ID</Label>
                            <Input
                                type="text"
                                name="productId"
                                id="productId"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="productName">Product Name</Label>
                            <Input
                                type="text"
                                name="productName"
                                id="productName"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input
                                type="text"
                                name="description"
                                id="description"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="quantity">quantity</Label>
                            <Input
                                type="text"
                                name="quantity"
                                id="quantity"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="price">price</Label>
                            <Input
                                type="text"
                                name="price"
                                id="price"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="catId">CatId</Label>
                            <Select style={{ width: '100%' }} type="select" name="catId" id="catId" onChange={this.changeCat}>
                                {
                                    catReducer.cat.map(data => (
                                        <MenuItem value={data.catId}>{data.catName}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormGroup>
                        <FormGroup>
                            <Label for="myfile">Thumnails</Label>
                            <Input type="file" multiple name="myFile" id="myfile" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="FileTong">Images</Label>
                            <Input type="file" name="FileTong" id="FileTong" />
                        </FormGroup>
                        <FormGroup>
                            <Label for="detail">Detail</Label>
                            <Input type="textarea" name="detail" id="detail" />
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" type="submit">
                                Save
                        </Button>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        GetAllCat: () => {
            dispatch(getCartAdminCallApi())
        },
        addProduct: (formData) => {
            dispatch(addProductApi(formData));
        }
    }
}

const mapStateToProps = state => {
    return {
        catReducer: state,
        isLoggedIn : state.auth
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductAdd);