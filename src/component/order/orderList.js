import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllProductAPI, deleteCatAPI, deleteProductApi, getOrderAction, getOrder, updateOrder } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css';
import { Redirect } from 'react-router-dom';

class OrderList extends Component {

  componentDidMount() {
    if (!this.props.isLoggedIn.isLoggedIn) {
      return <Redirect to="/admin/login" />;
    }
    this.props.fetchOrder();
  }

  async update(payload) {
    await this.props.updateOrder(payload);
    window.location.reload();
  }

  render() {

    const { orderReducer } = this.props;
    const newOrder = orderReducer?.order.filter(item => item.status !== 'shipped');
    const successOrder = orderReducer?.order.filter(item => item.status === 'shipped')
    console.log(this.props);
    return (
      <div className="App">
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform" >
          <div style={{ width: '20%', marginTop: '100px', border: '1px solid green', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: 'green', borderRadius: '10px', color: 'white' }}>Duyệt đơn hàng</div>
          <table style={{ width: '95%', marginTop: '10px' }} id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
            <thead>
              <tr>
                <th className="text-center">Customer</th>
                <th className="text-center">phone</th>
                <th className="text-center">Address</th>
                <th className="text-center">Product</th>
                <th className="text-center">Quantity</th>
                <th className="text-center">Total</th>
                <th className="text-center">Create at</th>
                <th className="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              {
                newOrder.map(data => (
                  <tr align="start">
                    <td className="text-center">{data.fullName}</td>
                    <td className="text-center">{data.phone}</td>
                    <td className="text-center">{data.address}</td>
                    <td className="text-center">{data.product_name}</td>
                    <td className="text-center">{data.quantity}</td>
                    <td className="text-center">{data.quantity * data.price}</td>
                    <td className="text-center">{data.createdAt}</td>
                    <td className="text-center">
                      <button type="button" style={{ backgroundColor: (data?.status === 'pending' ? 'green' : 'gray'), color: 'white' }} className="btn" onClick={() => this.update({ id: data.orderId, status: 'pending' })}>Pending</button>
                      <button type="button" style={{ backgroundColor: (data?.status === 'progress' ? 'green' : 'gray'), margin: '5px', color: 'white' }} className="btn" onClick={() => this.update({ id: data.orderId, status: 'progress' })}>Progress</button>
                      <button type="button" style={{ backgroundColor: (data?.status === 'shiped' ? 'green' : 'gray'), color: 'white' }} className="btn" onClick={() => this.update({ id: data.orderId, status: 'shipped' })}>Shiped</button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
          <div style={{ width: '20%', marginTop: '60px', border: '1px solid green', height: '50px', display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: 'green', borderRadius: '10px', color: 'white' }}>Đơn hàng đã giao</div>
          <table style={{ width: '95%', marginTop: '10px' }} id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
            <thead>
              <tr>
                <th className="text-center">Customer</th>
                <th className="text-center">phone</th>
                <th className="text-center">Address</th>
                <th className="text-center">Product</th>
                <th className="text-center">Quantity</th>
                <th className="text-center">Total</th>
                <th className="text-center">Create at</th>
              </tr>
            </thead>
            <tbody>
              {
                successOrder.map(data => (
                  <tr align="start">
                    <td className="text-center">{data.fullName}</td>
                    <td className="text-center">{data.phone}</td>
                    <td className="text-center">{data.address}</td>
                    <td className="text-center">{data.product_name}</td>
                    <td className="text-center">{data.quantity}</td>
                    <td className="text-center">{data.quantity * data.price}</td>
                    <td className="text-center">{data.createdAt}</td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div >
      </div >
    )
  }
}
const mapStateToProps = state => {
  return {
    orderReducer: state,
    isLoggedIn: state.auth
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchOrder: () => {
      dispatch(getOrder())
    },
    updateOrder: (id) => {
      dispatch(updateOrder(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);