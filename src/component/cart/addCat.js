import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { addCatAPI, getCatAPIById } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'
import jwt_decode from 'jwt-decode';
import { Redirect } from 'react-router-dom';

class AddCat extends Component {

    constructor(props) {
        super(props);
        this.setState({
            catId: '',
            catName: ''
        })
    }
    componentDidMount() {
        if(!this.props.isLoggedIn.isLoggedIn){
            return <Redirect to="/admin/login"/>;
        }
        const { match } = this.props
        if (match.params.catId) {
            this.props.getCatById(match.params.catId)
        }
    }

    // handleChange = (event) => {
    //     const target = event.target;
    //     const value = target.value;
    //     const name = target.name;
    //     this.setState({
    //         [name]: value,
    //     });
    //   };

    handelsubmit = (event) => {
        event.preventDefault();
        const cat = {
            catId: event.target.catId.value,
            catName: event.target.catName.value,
        }
        this.props.OnAddCat(cat)
        this.props.history.push('/admin/cat/index')
    }

    render() {
        return (
            <div>
                <DashboardNavbar />
                <DashboardSidebar />
                <div className="divform">
                    <Form onSubmit={this.handelsubmit}>
                        <FormGroup>
                            <Label for="catId">CatID</Label>
                            <Input
                                type="text"
                                name="catId"
                                id="fullName"
                                style={{
                                    width: '30%'
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="address">Categoties Name</Label>
                            <Input
                                type="text"
                                name="catName"
                                id="catName"
                                style={{
                                    width: '30%'
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Button color="primary" type="submit">
                                Save
                        </Button>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        OnAddCat: (cat) => {
            dispatch(addCatAPI(cat));
        },
        getCatById: (catId) => {
            dispatch(getCatAPIById(catId));
        }
    }
}

const mapStateToProps = state => {
    return {
        catReducer: state,
        isLoggedIn : state.auth
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCat);