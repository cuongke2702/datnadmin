import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCartAdminCallApi, deleteCatAPI } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'
import { Redirect } from 'react-router-dom';

class CatList extends Component {
  componentDidMount() {
    if (!this.props.isLoggedIn.isLoggedIn) {
      return <Redirect to="/admin/login" />;
    }
    this.props.fetchAPI();
  }
  delete = (id) => {
    this.props.deleteCat(id);
  }
  render() {
    const { catReducer } = this.props;
    return (
      <div className="App">
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform">
          <a style={{ marginTop: '100px', marginRight: '1183px', width: '200px' }} className="btn btn-primary" href={"/admin/cat/add"}>Thêm đanh mục</a>
          <table style={{ width: '95%', marginTop: '10px' }} id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
            <thead>
              <tr>
                <th className="text-center">ID</th>
                <th className="text-center">Tên đanh mục</th>
                <th className="text-center" colSpan={2}>Chức Năng</th>
                {/* <th className="text-center">Add</th> */}
                {/* <th className="text-center">Xóa</th> */}
              </tr>
            </thead>
            <tbody>
              {
                catReducer.cat.map(data => (
                  <tr align="start">
                    <td className="text-center">{data.catId}</td>
                    <td className="text-center">{data.catName}</td>
                    <td className="text-center">
                      <a className="btn btn-primary" href={"/admin/cat/edit/" + data.catId}>Cập nhật</a>
                    </td>
                    {/* <td className="text-center">
                      <a className="btn btn-primary" href="/admin/cat/add">Add</a>
                    </td> */}
                    <td className="text-center">
                      <button type="button" className="btn btn-danger" onClick={() => this.delete(data.catId)}>Xóa</button>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    catReducer: state,
    isLoggedIn: state.auth
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAPI: () => {
      dispatch(getCartAdminCallApi())
    },
    deleteCat: (id) => {
      dispatch(deleteCatAPI(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CatList);