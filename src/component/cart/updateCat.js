import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Form, FormGroup, Input, Label } from "reactstrap";
import { getCatAPIById, UpdateCatAPI } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'
import jwt_decode from 'jwt-decode';
import { Redirect } from 'react-router-dom';

class UpdateCat extends Component {

  constructor(props) {
    super(props);
    this.setState({
      catId: '',
    })
  }
  componentDidMount() {
    if (!this.props.isLoggedIn.isLoggedIn) {
      return <Redirect to="/admin/login" />;
    }
    const { match } = this.props
    this.props.getCatById(match.params.catId)
    this.setState({
      catId: match.params.catId,
    })
  }

  // handleChange = (event) => {
  //     const target = event.target;
  //     const value = target.value;
  //     const name = target.name;
  //     this.setState({
  //         [name]: value,
  //     });
  //   };

  handelsubmit = (event) => {
    const cat = {
      catId: this.state.catId,
      catName: event.target.catName.value,
    }
    this.props.OnUpdate(cat);
    this.props.history.push('/admin/cat/index')
  }

  render() {
    const { editReducer } = this.props
    const { edit } = editReducer
    return (
      <div>
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform">
          <Form onSubmit={this.handelsubmit}>
            <FormGroup>
              <Label for="catId">Name of category</Label>
              <Input
                type="text"
                name="catName"
                id="catName"
                defaultValue={edit.catName}
                style={{
                  width: '30%'
                }}
              />
            </FormGroup>
            <FormGroup>
              <Button color="primary" type="submit">
                Save
              </Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    getCatById: (catId) => {
      dispatch(getCatAPIById(catId));
    },
    OnUpdate: (cat) => {
      dispatch(UpdateCatAPI(cat))
    }
  }
}

const mapStateToProps = state => {
  return {
    editReducer: state,
    isLoggedIn: state.auth
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateCat);