import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCartAdminCallApi, deleteCatAPI, getAllComment, getNumberCommentAPI } from '../../actions';
import DashboardNavbar from '../LayOut/DashboardNavbar';
import DashboardSidebar from '../LayOut/DashboardSidebar';
import '../../Css/content.css'
import { Redirect } from 'react-router-dom';
import ReactPaginate from 'react-paginate';

class commentList extends Component {
  componentDidMount() {
    if (!this.props.isLoggedIn.isLoggedIn) {
      return <Redirect to="/auth/login" />;
    }
    this.props.fetchAPI(1);
    this.props.getcountComment();
  }


  handlePageChange = (event) => {
    let number=event.selected
    this.props.fetchAPI(number)
};

  render() {
    const { catReducer } = this.props;
    let count=catReducer?.number
    let pageNumber=count/60
    return (
      <div className="App">
        <DashboardNavbar />
        <DashboardSidebar />
        <div className="divform">
          
          <table style={{ width: '95%', marginTop: '100px' }} id="dsMay" className="table table-striped table-bordered dt-responsive nowrap cell-border">
            <thead>
              <tr>
                <th className="text-center">ID</th>
                <th className="text-center">FullName</th>
                <th className="text-center">Comment</th>
                {/* <th className="text-center">Add</th> */}
                <th className="text-center">rate</th>
              </tr>
            </thead>
            <tbody>
              {
                catReducer?.comment.map(data => (
                  <tr align="start">
                    <td className="text-center">{data.commentId}</td>
                    <td className="text-center">{data.fullName}</td>
                    <td ><div className='text-maxlength' style={{width:'300px'}} >{data.context}</div></td>
                    <td className="text-center">{data.rate}</td>
                  </tr>
                ))
              }
            </tbody>
          </table>
          <ReactPaginate
            breakLabel="..."
            nextLabel="next >"
            onPageChange={this.handlePageChange}
            pageRangeDisplayed={5}
            pageCount={pageNumber}
            previousLabel="< previous"
            renderOnZeroPageCount={null}
            containerClassName="pagination justify-content-center"
            pageClassName="page-item"
            pageLinkClassName="page-link"
            previousClassName="page-item"
            previousLinkClassName="page-link"
            nextClassName="page-item"
            nextLinkClassName="page-link"
            activeClassName="active"
        />
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    catReducer: state,
    isLoggedIn: state.auth
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAPI: (number) => {
      dispatch(getAllComment(number))
    },
    getcountComment : ()=> {
      dispatch(getNumberCommentAPI())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(commentList);