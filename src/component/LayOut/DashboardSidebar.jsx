import { Component } from "react";
import { Link as RouterLink, useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  ListItem,
  Button,
  Divider,
  Drawer,
  List,
  Typography,
  Link
} from '@material-ui/core';
import {
  AlertCircle as AlertCircleIcon,
  BarChart as BarChartIcon,
  Lock as LockIcon,
  Settings as SettingsIcon,
  ShoppingBag as ShoppingBagIcon,
  User as UserIcon,
  UserPlus as UserPlusIcon,
  Users as UsersIcon
} from 'react-feather';

const items = [
  {
    href: '/admin/cat/index',
    icon: UsersIcon,
    title: 'Categories'
  },
  {
    href: '/admin/product/index',
    icon: ShoppingBagIcon,
    title: 'Products'
  },
  {
    href: '/admin/user/index',
    icon: UserIcon,
    title: 'Account'
  },
  {
    href: '/admin/order',
    icon: UserIcon,
    title: 'Orders'
  },
  {
    href: '/admin/comment',
    icon: UserIcon,
    title: 'Comment'
  },
];

class DashboardSidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: JSON.parse(localStorage.getItem('tab'))
    }
    this.handleListItemClick = this.handleListItemClick.bind(this);
  }

  handleListItemClick = (event, index) => {
    this.setState({ selectedIndex: index });
    localStorage.setItem('tab', index)
  };
  render() {
    return (
      <>
        <Drawer
          anchor="left"
          open
          variant="persistent"
          PaperProps={{
            sx: {
              width: 256,
              top: 64,
              height: 'calc(100% - 64px)'
            }
          }}
        >
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
            }}
          >
            <Divider />
            <div>
              <img src='../images/logo.png' style={{height:'93px',backgroundColor:'cyan',width:'300px'}}></img>
            </div>
            <Box sx={{ p: 2 }} style={{ width: '300px', paddingTop: '100px' }}>
              <List>
                {items.map((item, index) => (
                  <ListItem onClick={(e) => this.handleListItemClick(e, index)} selected={this.state.selectedIndex === index}>
                    <a style={{ width: '100%' }} class="nav-link collapsed" href={item.href} data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                      <span>{item.title}</span>
                    </a>
                  </ListItem>

                ))}
              </List>
            </Box>
            <Box sx={{ flexGrow: 1 }} />
            <Box
              sx={{
                backgroundColor: 'background.default',
                m: 2,
                p: 2
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  justifyContent: 'center',
                  pt: 2
                }}
              >
              </Box>
            </Box>
          </Box>
        </Drawer>
      </>
    );
  };
}
export default DashboardSidebar;