import { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import jwt_decode from "jwt-decode";
import UserList from "../user/userList";
import DashboardNavbar from "./DashboardNavbar";
import DashboardSidebar from "./DashboardSidebar";


class DashboardLayoutRoot extends Component {

  componentDidMount() {
    // console.log(this.props);
    if (!this.props.isLoggedIn.isLoggedIn) {
      this.props.history.push('/admin/login')
    }
  }

  render() {
    return (
      <>
        <DashboardNavbar />
        <DashboardSidebar />
        <UserList></UserList>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth
  };
};
export default connect(mapStateToProps, null)(DashboardLayoutRoot);