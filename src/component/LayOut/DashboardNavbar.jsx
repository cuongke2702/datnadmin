import { Component } from "react";
import { Link as RouterLink, Redirect } from 'react-router-dom';
import {
  AppBar,
  Badge,
  Box,
  Button,
  Hidden,
  IconButton,
  Link,
  Toolbar
} from '@material-ui/core';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import InputIcon from '@material-ui/icons/Input';
import { connect } from "react-redux";
import jwt_decode from 'jwt-decode';

class DashboardNavbar extends Component {

  constructor(props) {
    super(props);
    this.setState({
      user: '',
      anchorEl: null
    })
  }

  componentDidMount() {
    if (localStorage.getItem('user')) {
      const { token } = JSON.parse(localStorage.getItem('user'))
      const decode = jwt_decode(token);
      this.setState({ user: decode?.sub })
    }

  }
  handleClick = (event) => {
    this.setState({ ...this.state, anchorEl: event.currentTarget });
  };
  handleClose = () => {
    this.setState({ ...this.state, anchorEl: null });
  };
  logout = () => {
    console.log(this.tate);
    localStorage.removeItem('user');
    localStorage.removeItem('tab');
    return <Redirect to="/admin/login" />;
  }
  render() {
    return (
      <AppBar
        elevation={0}
      >
        <Toolbar style={{ display: 'flex', justifyContent: 'flex-end',height:'93px' }}>
          <div>
            <Button onClick={(e) => this.handleClick(e)}>{this.state?.user}</Button>
            <Menu
              id="simple-menu"
              anchorEl={this.state?.anchorEl}
              keepMounted
              open={Boolean(this.state?.anchorEl)}
              onClose={this.handleClose}
            >
              <MenuItem onClick={this.logout}><a href='/admin/login'>Logout</a></MenuItem>
            </Menu>
          </div>
          <Box sx={{ flexGrow: 1 }} />
          <Hidden lgDown>
            <IconButton color="inherit">
              <Badge
                color="primary"
                variant="dot"
              >
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton color="inherit">
              <InputIcon />
            </IconButton>
          </Hidden>
          <Hidden lgUp>
            <IconButton
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
          </Hidden>
        </Toolbar>
      </AppBar>
    );
  };
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.auth
  }
}
export default connect(mapStateToProps, null)(DashboardNavbar);