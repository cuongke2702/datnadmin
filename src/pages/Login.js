import jwt_decode from 'jwt-decode';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { loginUser } from "../actions";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class Login extends Component {
  constructor(props) {
    super(props);
    this.setState({
      username: '',
      password: ''
    })
  }

  onSubmit = async (e) => {
    e.preventDefault();
    await this.props.loginUser(this.state);

  }

  handleChange = (e) => {
    this.setState({ ...this.state, [e.target.name]: e.target.value })
  }

  render() {
    const { isLoggedIn } = this.props
    if (isLoggedIn.isLoggedIn && localStorage.getItem('user')) {
      const decode = jwt_decode(isLoggedIn?.user?.token || isLoggedIn?.token)
      if (decode.roles === "ROLE_USER") {
        toast("Access denied!");
        localStorage.removeItem('user')
      } else {
        localStorage.setItem('tab', 2)
        return <Redirect to="/admin/home" />;
      }
    }
    return (
      <div style={{ width: '100vw', height: '100vh', backgroundColor: '#f2f2f2', display: 'flex' }}>
        <form style={{ width: '350px', height: '50%', borderRadius: '10px', backgroundColor: 'white', margin: 'auto' }}>
          <div style={{ fontSize: '24px', fontWeight: 'bold', textAlign: 'center', margin: '20px 0 30px 0' }}>Login</div>
          <div style={{ display: 'flex', marginBottom: '20px', justifyContent: 'center' }}>
            <div style={{ width: '90px' }}>
              <span>Username:</span>
            </div>
            <input style={{ outline: 'none', border: 'none', backgroundColor: '#f2f2f2', borderRadius: '5px', padding: '5px', width: '60%' }} onChange={this.handleChange} name="username" />
          </div>
          <div style={{ display: 'flex', justifyContent: 'center' }}>
            <div style={{ width: '90px' }}>
              <span>Password:</span>
            </div>
            <input type='password' style={{ outline: 'none', border: 'none', backgroundColor: '#f2f2f2', borderRadius: '5px', padding: '5px', width: '60%' }} onChange={this.handleChange} name="password" />
          </div>
          <button type='submit' style={{ width: '100px', height: 'fit-content', padding: '5px 15px', backgroundColor: 'pink', borderRadius: '5px', color: 'white', display: 'flex', justifyContent: 'center', margin: '0 auto', marginTop: '50px', cursor: 'pointer', outline: 'none' }} onClick={(e) => this.onSubmit(e)}>Submit</button>
        </form>
        <ToastContainer />
      </div>
    );
  }
};


const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    loginUser: (user) => {
      dispatch(loginUser(user));
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Login);