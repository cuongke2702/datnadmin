import * as Types from "../utils/ActionTypes";
import callAPiproduct from "../utils/callAPiproduct";
import callApi from "./../utils/apiCaller";

export const getUserAdmin = (user) => {
    return {
        type: Types.FETCH_USER,
        user,
    };

};

export const delUser = (id) => {
    return {
        type: Types.DELETE_USER,
        id,
    };
};

export const loginUserSuccess = (user) => {
    return {
        type: Types.LOGIN,
        user,
    };
};

export const loginUser = (user) => {
    return dispatch => {
        return callApi(`auth/login`, 'POST', user).then(res => {
            if (res.data.data) {
                localStorage.setItem("user", JSON.stringify(res.data.data));
            }
            dispatch(loginUserSuccess(res.data.data));
        }).catch(error => {
            console.log(error.response)
        })
    }
}

export const deleteUser = (id) => {
    return dispatch => {
        return callApi(`api/admin/user/del/${id}`, 'DELETE', null).then(res => {
            dispatch(delUser(id));
        })
    };
}

export const deleteCart = (id) => {
    return {
        type: Types.DELETE_CAT,
        id,
    };
};

export const deleteCatAPI = (id) => {
    return dispatch => {
        return callApi(`api/admin/cat/del/${id}`, 'DELETE', null).then(res => {
            dispatch(deleteCart(id));
        })
    };
}

export const getUserAdminCallApi = () => {
    return dispatch => {
        return callApi(`api/admin/user/index`, 'GET', null).then(res => {
            dispatch(getUserAdmin(res.data.data));
        })
    }
}

export const getCartAdmin = (cat) => {
    return {
        type: Types.FETCH_CAT,
        cat,
    };

};

export const getCartAdminCallApi = () => {
    return dispatch => {
        return callApi(`api/admin/cat/index`, 'GET', null).then(res => {
            dispatch(getCartAdmin(res.data.data));
        })
    }
}

export const getComment = (comment) => {
    return {
        type: Types.FETCH_COMMENT,
        comment,
    };

};

export const getAllComment = (numberComment) => {
    return dispatch => {
        return callApi(`api/comment/commentpagination?numberComment=${numberComment}`, 'GET', null).then(res => {
            dispatch(getComment(res.data.data));
        })
    }
}


export const updateOrderAction = (order) => {
    return {
        type: Types.UPDATE_ORDER,
        order,
    }
}

export const updateOrder = (payload) => {
    return async (dispatch) => {
        const res = await callApi(`api/order/updatestatus/${payload.id}`, 'PUT', {
            status: payload.status,
        });
        dispatch(updateOrderAction(res.data.data));
    };
}

export const getOrderAction = (order) => {
    return {
        type: Types.FETCH_ORDER,
        order,
    }
}

export const getOrder = () => {
    return async (dispatch) => {
        const res = await callApi(`api/order/getallorders`, 'GET', null);
        dispatch(getOrderAction(res.data.data));
    };
}

export const getCatById = (cat) => {
    return {
        type: Types.GET_CAT,
        cat,
    }
}

export const getCatAPIById = (catId) => {
    return dispatch => {
        return callApi(`api/admin/cat/getcat/${catId}`, 'GET', null).then(res => {
            console.log('abc', res.data);
            dispatch(getCatById(res.data.data))
        })
    }
}

export const UpdateCatAPI = (cat) => async dispatch => {
    try {
        const { data } = await callApi(`api/admin/cat/edit/${cat.catId}`, 'POST', cat)
        console.log(data.data);
        return dispatch(updateCatById(data.data))
    } catch (error) {
        console.log(error.response)
    }
}


export const deleteProduct = (id) => {
    return {
        type: Types.DELETE_PRODUCT,
        id,
    };
};

export const deleteProductApi = (id) => {
    return dispatch => {
        return callApi(`api/admin/product/deleteProductById/${id}`, 'DELETE', null).then(res => {
            dispatch(deleteProduct(id));
        })
    };
}

export const getFindAllProduct = (products) => {
    return {
        type: Types.FETCH_PRODUCT,
        products,
    }
}

export const getAllProductAPI = () => {
    return dispatch => {
        return callApi(`api/admin/product/getListProduct`, 'GET', null).then(res => {
            dispatch(getFindAllProduct(res.data.data))
        })
    }
}


export const getPagition = (products) => {
    return {
        type: Types.PAGE_PRODUCT,
        products,
    }
}

export const getPagitions = (pageNumber) => {
    return dispatch => {
        return callApi(`api/product/getproductpageble?pageNumber=${pageNumber}`, 'GET', null).then(res => {
            dispatch(getPagition(res.data.data))
        })
    }
}


export const getCountProduct = (number) => {
    return {
        type: Types.NUMBER,
        number,
    }
}

export const getNumberAPI = () => {
    return dispatch => {
        return callApi(`api/product/count`, 'GET', null).then(res => {
            dispatch(getCountProduct(res.data.data))
        })
    }
}


export const getCountComment = (number) => {
    return {
        type: Types.NUMBER,
        number,
    }
}

export const getNumberCommentAPI = () => {
    return dispatch => {
        return callApi(`api/comment/countcomment`, 'GET', null).then(res => {
            dispatch(getCountComment(res.data.data))
        })
    }
}






export const addProductAction = (product) => {
    return {
        type: Types.ADD_PRODUCT,
        product,
    }
}

export const addProductApi = (formdata) => {
    return dispatch => {
        return callApi(`api/admin/product/uploadFile`, 'POST', formdata).then(res => {
            console.log(res.data.data);
            dispatch(addProductAction(res.data.data))
        })
    }
}

export const updateProductAction = (product) => {
    return {
        type: Types.UPDATE_PRODUCT,
        product,
    }
}

export const updateProductApi = (formdata, id) => {
    return dispatch => {
        return callApi(`api/admin/product/updateProduct/${id}`, 'POST', formdata).then(res => {
            dispatch(updateProductAction(res.data.data))
        })
    }
}

export const getDetailProductAction = (product) => {
    return {
        type: Types.GET_PRODUCT_BY_ID,
        product,
    }
}

export const getDetailProductApi = (productId) => {
    return dispatch => {
        return callApi(`api/product/findProductById/${productId}`, 'GET', null).then(res => {
            dispatch(getDetailProductAction(res.data.data));
        })
    }
}

export const getUserById = (user) => {
    return {
        type: Types.GET_USER_BY_ID,
        user,
    }
}

export const getUserAPIRequest = (id) => {
    return dispatch => {
        return callApi(`api/admin/user/getuser/${id}`, 'GET', null).then(res => {
            dispatch(getUserById(res.data.data));
        })
    }
}

export const updateUserById = (user) => {
    return {
        type: Types.UPDATE_USER,
        user,
    }
}

export const updateUserAPICall = (user) => {
    return dispatch => {
        return callApi(`api/admin/user/update/${user.userId}`, 'PUT', user).then(res => {
            dispatch(updateUserById(res.data.data))
        })
    }
}

export const addCat = (cat) => {
    return {
        type: Types.ADD_CAT,
        cat,
    }
}

export const addCatAPI = (cat) => {
    return dispatch => {
        return callApi(`api/admin/cat/add`, 'POST', cat).then(res => {
            dispatch(addCat(res.data.data))
        })
    }
}


export const updateCatById = (cat) => {
    return {
        type: Types.UPDATE_CAT,
        cat,
    }
}










