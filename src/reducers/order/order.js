import * as Types from "../../utils/ActionTypes";

const initialState = [];

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_ORDER: {
      state = [...action.order]
      return [...state];
    }
    case Types.DELETE_ORDER: {
      const newStateValue = []
      state.forEach(each => {
        if (each.catId.toString() !== action.id.toString()) {
          newStateValue.push(each)
        }
      })
      return [...newStateValue]
    }
    case Types.UPDATE_ORDER: {
      state = [...state];
      return [...state]
    }
    default:
      return state;
  }
}

export default orderReducer;