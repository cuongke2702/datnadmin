import * as Types from "../../utils/ActionTypes";

const initialState = [];

const catReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_CAT: {
      state = [...action.cat]
      return [...state];
    }
    case Types.DELETE_CAT: {
      const newStateValue = []
      state.forEach(each => {
        if (each.catId.toString() !== action.id.toString()) {
          newStateValue.push(each)
        }
      })
      return [...newStateValue]
    }
    case Types.ADD_CAT: {
      state.push(action.cat)
      return [...state]
    }
    case Types.UPDATE_CAT: {
      state = [...state];
      const isExitCat = state.find(item => item.catId.toString() === action.cat.catId.toString())
      console.log(isExitCat);
      if (isExitCat) {
        state = state.map(each => {
          if (each.catId.toString() === action.cat.catId.toString()) {
            return {
              ...each,
              catName: action.cat.catName,
            }
          }
          return each
        })
      }
      if (!isExitCat) {
        state.push(action.cat)
      }
      return [...state]
    }
    default:
      return [...state];
  }
}

export default catReducer;