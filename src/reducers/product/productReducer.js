import * as Types from "../../utils/ActionTypes";

const initialState = [];

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_PRODUCT: {
      state = [...action.products]
      return [...state];
    }
    case Types.PAGE_PRODUCT: {
      state = [...action.products]
      return [...state];
    }
    case Types.GET_PRODUCT_BY_ID: {
      state.product = action.product
      return state;
    }
    case Types.DELETE_PRODUCT: {
      const newStateValue = []
      state.forEach(each => {
        if (each.productId.toString() !== action.id.toString()) {
          newStateValue.push(each)
        }
      })
      return [...newStateValue]
    }
    case Types.ADD_PRODUCT: {
      state.push(action.product)
      return [...state]
    }
    case Types.UPDATE_PRODUCT: {
      state = state;
      return state;
    }
    default:
      return state;
  }
}

export default productReducer;