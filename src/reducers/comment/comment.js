import * as Types from "../../utils/ActionTypes";

const initialState = [];

const commentReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_COMMENT: {
      state = [...action.comment]
      return [...state];
    }
    default:
      return [...state];
  }
}

export default commentReducer;