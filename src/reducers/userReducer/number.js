import * as Types from "../../utils/ActionTypes";

const initialState = {};

const numberReducer =  (state = initialState,action)=>{
    switch(action.type){
        case Types.NUMBER: {
          state= action.number;
          return state
        }
        default:
          return state;
      }
}

export default numberReducer;