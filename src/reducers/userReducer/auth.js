import * as Types from "../../utils/ActionTypes";

const user = JSON.parse(localStorage.getItem("user"));

var initialState = user
? { isLoggedIn: true, user }
: { isLoggedIn: false, user: null };

const authReceduce = (state = initialState, action) => {
  switch (action.type) {
    case Types.LOGIN:
      return {
          ...state,
          isLoggedIn: true,
          token:action.user.token
      };
    case Types.LOGOUT:
      return {
          ...state,
          isLoggedIn: false,
          token:null,
      };
    default:
      return {...state};
  }
}

export default authReceduce;