import * as Types from "../../utils/ActionTypes";

const initialState = [];

const userReceduce = (state = initialState, action) => {
  switch (action.type) {
    case Types.FETCH_USER: {
      state = [...action.user]
      return [...state];
    }
    case Types.DELETE_USER: {
      const newStateValue = []
      state.forEach(each => {
        if (each.userId.toString() !== action.id.toString()) {
          newStateValue.push(each)
        }
      })
      return [...newStateValue]
    }
    case Types.UPDATE_USER: {
      state = [...state];
      const isExitUser = state.find(item => item.userId.toString() === action.user.userId.toString())
      if (isExitUser) {
        state = state.map(each => {
          if (each.userId.toString() === action.user.userId.toString()) {
            return {
              ...each,
              fullName: action.user.fullName,
              phone: action.user.phone,
              address: action.user.address,
              email: action.user.email
            }
          }
          return each
        })
      }
      if (!isExitUser) {
        state.push(action.product)
      }
      return [...state]
    }
    default:
      return [...state];
  }
}

export default userReceduce;