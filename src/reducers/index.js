import { combineReducers } from 'redux';
import userReceduce from './userReducer/user';
import catReducer from './cart/cat';
import editReducer from './userReducer/editUser';
import productReducer from './product/productReducer';
import authReducer from './userReducer/auth';
import orderReducer from './order/order';
import commentReducer from './comment/comment';
import numberReducer from './userReducer/number';
const allReducers = combineReducers({

    user: userReceduce,
    cat: catReducer,
    edit: editReducer,
    product: productReducer,
    auth: authReducer,
    order: orderReducer,
    comment: commentReducer,
    number: numberReducer
});
export default allReducers;