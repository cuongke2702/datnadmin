export const FETCH_USER = 'FETCH_USER';

export const DELETE_USER = 'DELETE_USER';

export const DELETE_CAT = 'DELETE_CAT';

export const UPDATE_USER = 'UPDATE_USER';

export const ADD_CAT = 'ADD_CAT';

export const GET_USER_BY_ID = 'GET_USER_BY_ID';
export const NUMBER = 'NUMBER';

export const FETCH_CAT = 'FETCH_CAT1';

export const FETCH_COMMENT = 'FETCH_COMMENT';

export const GET_CAT = 'GET_CAT';

export const UPDATE_CAT = 'UPDATE_CAT';

export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';

export const FETCH_PRODUCT = 'FETCH_PRODUCT';

export const PAGE_PRODUCT = 'PAGE_PRODUCT';


export const DLOAD_PRODUCT = 'DLOAD_PRODUCT';

export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const UPDATE_ORDER = 'UPDATE_ORDER';

export const FETCH_ORDER = 'FETCH_ORDER';

export const DELETE_ORDER = 'DELETE_ORDER';
export const GET_PRODUCT_BY_ID = 'GET_PRODUCT_BY_ID';
export const ADD_PRODUCT = 'ADD_PRODUCT';

export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';