import axios from 'axios';

export default function callApiHeder(endpoint, method = 'GET', body, token) {
    return axios({
        method: method,
        url: `${Config.API_URL}/${endpoint}`,
        data: body,
        headers: {
            Authorization: 'Bearer ' + token
        }
    })

};